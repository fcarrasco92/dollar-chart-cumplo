## Get dollar and generate chart

#### Example:

dollar-chart-cumplo

An application to show a graph with the values of the dollar between two dates, in addition to showing the minimum, maximum and average value. built with React, JavaScript, CSS(styled-components), docker, now.sh for deploy, Gitlab.

## Installation and Setup Instructions

Clone down this repository. You will need `node` and `npm` installed globally on your machine.

Installation:

`npm install`

Configurate .env file:

```
KEY_SBIF=xxxxxxxxx
```

To Start Server:

`npm start` or `npm run dev`

To Visit App:

`localhost:8080`

# Run in Docker

Previously you must have installed docker in your machine

Installation:

`npm run docker-build`

To Start Server in Docker:

`npm run docker-run`

To Visit App:

`localhost:8080`

## Deploy in `now.sh`

To deploy in this service you must have the credentials to connect.
You should also have add `apiKey` environment variable in the file `now.json`

```script
"build": {
	"env": {
		"KEY_SBIF": ""
	}
}
```

```script
	npm run build-now
```

you can check in this url: https://dollar-chart.fcarrasco93.now.sh/
