export default {
  getDollarByDates: (startDate = "", endDate = "") => {
    const startDateSplit = startDate.split("-");
    const endDateSplit = endDate.split("-");
    const apiKey = process.env.KEY_SBIF;
    return `https://api.sbif.cl/api-sbifv3/recursos_api/dolar/periodo/${startDateSplit[0]}/${startDateSplit[1]}/dias_i/${startDateSplit[2]}/${endDateSplit[0]}/${endDateSplit[1]}/dias_f/${endDateSplit[2]}?apikey=${apiKey}&formato=json`;
  },
};
