import styled from "styled-components";

export const Title = styled.h1`
  font-size: 1.5em;
  text-align: center;
  color: white;
`;

export const BannerHeader = styled.div`
  box-sizing: border-box;
  width: 100%;
  display: flex;
  justify-content: center;
  padding: 1em;
  margin-bottom: 2em;
  background-color: #00ba68;
  color: #00ba68;
`;
